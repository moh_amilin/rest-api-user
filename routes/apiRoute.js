const express = require('express');
const router = express.Router();
const { authJwt } = require("../middleware");
const {
    allUserAccess,
    userAccess,
    adminAccess,
} = require("../controllers/users");
const { viewUser } = require("../controllers/auths");

// versi 1
router.get("/data/user", viewUser);
router.get("/all", allUserAccess);
router.get("/user", [authJwt.checkToken], userAccess);
router.get("/admin", [authJwt.checkToken, authJwt.checkAdmin], adminAccess);
module.exports = router;