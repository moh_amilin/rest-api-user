module.exports = {
    allUserAccess: (req, res) => {
        res.status(200).send("public content")
    },
    adminAccess: (req, res) => {
        res.status(200).send("admin content")
    },
    userAccess: (req, res) => {
        res.status(200).send("user content")
    }
}